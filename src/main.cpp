#include "../headder/raylib.h"

// Screen
int screen_witdh = 800;
int screen_height = 600;

// Ball
struct Ball
{
	float x, y;
	float speed_x, speed_y;
	float radius;

	void Draw()
	{
		DrawCircle((int)x, (int)y, radius, WHITE);
	};
};

struct Court
{
	void Draw()
	{
		// Draw court
		// Vertical lines
		DrawRectangle(0, 0, 5, screen_height, DARKGRAY);
		DrawRectangle(screen_witdh / 4, 50, 5, screen_height - 100, DARKGRAY);
		DrawRectangle(screen_witdh - screen_witdh / 4, 50, 5, screen_height - 100, DARKGRAY);
		DrawRectangle(screen_witdh - 5, 0, 5, screen_height, DARKGRAY);
		// Horizontal lines
		DrawRectangle(0, 0, screen_witdh, 5, DARKGRAY);
		DrawRectangle(0, 50, screen_witdh, 5, DARKGRAY);
		DrawRectangle(0, screen_height - 50, screen_witdh, 5, DARKGRAY);
		DrawRectangle(0, screen_height - 5, screen_witdh, 5, DARKGRAY);
		DrawRectangle(screen_witdh / 4, screen_height / 2, screen_witdh - screen_witdh / 2, 5, DARKGRAY);
	};
};

struct Paddle
{
	float x, y;
	float speed;
	float width, height;

	Rectangle GetRectangle()
	{
		return Rectangle{ x - width / 2, y - height / 2, width, height };
	};

	void Draw()
	{
		DrawRectangleRec(GetRectangle(), WHITE);
	};
};

int main()
{

	InitWindow(screen_witdh, screen_height, "Pong");
	SetWindowState(FLAG_VSYNC_HINT);

	Court court;

	Ball ball;
	ball.x = screen_witdh / 2.0f;
	ball.y = screen_height / 2.0f;
	ball.radius = 5;
	ball.speed_y = 150;
	ball.speed_x = 250;

	Paddle left_paddle, right_paddle;
	left_paddle.x = 50;
	left_paddle.y = GetScreenHeight() / 2;
	left_paddle.width = 10;
	left_paddle.height = 100;
	left_paddle.speed = 500;

	right_paddle.x = GetScreenWidth() - 50;
	right_paddle.y = GetScreenHeight() / 2;
	right_paddle.width = 10;
	right_paddle.height = 100;
	right_paddle.speed = 500;

	const char* winner_text = nullptr;
 
	while (!WindowShouldClose())
	{
		// Update the ball position
		ball.x += ball.speed_x * GetFrameTime();
		ball.y += ball.speed_y * GetFrameTime();

		// Move the ball
		if (ball.y <= 0)
		{
			ball.y = 0;
			ball.speed_y *= -1;
		};
		if (ball.y >= GetScreenHeight())
		{
			ball.y = GetScreenHeight();
			ball.speed_y *= -1;
		};

		// Player input
		if (IsKeyDown(KEY_W))
		{
			left_paddle.y -= left_paddle.speed * GetFrameTime();
		}		
		if (IsKeyDown(KEY_S))
		{
			left_paddle.y += left_paddle.speed * GetFrameTime();
		}
		if (IsKeyDown(KEY_UP))
		{
			right_paddle.y -= right_paddle.speed * GetFrameTime();
		}
		if (IsKeyDown(KEY_DOWN))
		{
			right_paddle.y += right_paddle.speed * GetFrameTime();
		}

		// Check for collision
		if (CheckCollisionCircleRec(Vector2{ ball.x, ball.y }, ball.radius, left_paddle.GetRectangle()))
		{
			if (ball.speed_x < 0)
			{
				ball.speed_x *= -1.1f;
				ball.speed_y = (ball.y - left_paddle.y) / (left_paddle.y / 2) * ball.speed_x;
			}

		};
		if (CheckCollisionCircleRec(Vector2{ ball.x, ball.y }, ball.radius, right_paddle.GetRectangle()))
		{
			if (ball.speed_x > 0)
			{
				ball.speed_x *= -1.1f;
				ball.speed_y = (ball.y - right_paddle.y) / (right_paddle.y / 2) * -ball.speed_x;
			};
		};

		if (ball.x < 0)
		{
			winner_text = "Right Player Wins!";
		};
		if (ball.x > GetScreenWidth())
		{
			winner_text = "Left Player Wins!";
		}

		if (winner_text && IsKeyPressed(KEY_SPACE))
		{
			ball.x = screen_witdh / 2.0f;
			ball.y = screen_height / 2.0f;
			ball.radius = 5;
			ball.speed_y = 150;
			ball.speed_x = 250;
			winner_text = nullptr;
		}

		// Setup Canvas(framebuffer) to start drawing
		BeginDrawing();
		// Set background color (framebuffer clear color)
		ClearBackground(DARKGREEN);

		// Draw court
		court.Draw();
		// Draw ball
		ball.Draw();
		// Draw paddles
		left_paddle.Draw();
		right_paddle.Draw();

		if (winner_text)
		{
			int text_width = MeasureText(winner_text, 60);
			DrawText(winner_text, GetScreenWidth() / 2 - text_width / 2, GetScreenHeight() / 2 - 30, 60, YELLOW);
		}

		DrawFPS(10, 10);
		// End canvas drawing and swap buffers (double buffering)
		EndDrawing();
	}

	CloseWindow();

	return 0;
}
